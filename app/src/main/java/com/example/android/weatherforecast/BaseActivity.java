package com.example.android.weatherforecast;

import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import rx.Scheduler;
import rx.Subscription;

/**
 * Created by pratama on 7/16/2016.
 */
public class BaseActivity extends AppCompatActivity {

    private List<Subscription> subscriptions = new ArrayList<>();
    protected Scheduler scheduler;


    protected void addSubscription(Subscription subscription) {
        subscriptions.add(subscription);
    }

    public void finishSubscriber() {
        for (Subscription sub : subscriptions) {
            sub.unsubscribe();
        }
    }


}
