package com.example.repository;

import com.example.BaseApi;
import com.example.dao.WeatherApiDao;

import java.util.HashMap;

import rx.Observable;


public class WeatherRepository {
    BaseApi mBaseApi;
    private String q;
    private String appid;

    public WeatherRepository(BaseApi mBaseApi, String q, String appid) {
        this.mBaseApi = mBaseApi;
        this.q = q;
        this.appid = appid;
    }

    public Observable<WeatherApiDao> getData() {
        return mBaseApi.getService().getWeather(q, appid);
    }
}
