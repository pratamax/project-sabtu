package com.example;

import com.example.dao.WeatherApiDao;

import java.util.Map;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;


public interface ApiService {

    //q=Jakarta&appid=eeccee48ca9437fce0c49fdcbbc8955a
    @GET("weather")
    Observable<WeatherApiDao> getWeather(
            @Query("q") String q,
            @Query("appid") String appid
    );

}